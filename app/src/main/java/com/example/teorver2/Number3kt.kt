package com.example.teorver2

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.Window
import android.view.WindowManager
import kotlinx.android.synthetic.main.activity_number3.*
import java.math.BigDecimal
import java.math.RoundingMode

class Number3kt : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_number3)
        var count: Int = 5
        count_events_ok.setOnClickListener{
            try {
                count = count_events.text.toString().toInt()
                if(count>5)
                    count = 5
                if(count<2)
                    count=2
            }
            catch (e:NumberFormatException){
                count = 5
            }
            when (count){
                4 -> {
                    ph5t.visibility= View.GONE
                    ph5.visibility=View.GONE
                    ph5аt.visibility= View.GONE
                    ph5а.visibility= View.GONE
                    ph3t.visibility= View.VISIBLE
                    ph3.visibility=View.VISIBLE
                    ph3аt.visibility= View.VISIBLE
                    ph3а.visibility= View.VISIBLE
                    ph4t.visibility= View.VISIBLE
                    ph4.visibility=View.VISIBLE
                    ph4аt.visibility= View.VISIBLE
                    ph4а.visibility= View.VISIBLE
                }
                3 -> {
                    ph5t.visibility= View.GONE
                    ph5.visibility=View.GONE
                    ph5аt.visibility= View.GONE
                    ph5а.visibility= View.GONE
                    ph4t.visibility= View.GONE
                    ph4.visibility=View.GONE
                    ph4аt.visibility= View.GONE
                    ph4а.visibility= View.GONE
                    ph3t.visibility= View.VISIBLE
                    ph3.visibility=View.VISIBLE
                    ph3аt.visibility= View.VISIBLE
                    ph3а.visibility= View.VISIBLE
                }
                2 ->{
                    ph3t.visibility= View.GONE
                    ph3.visibility=View.GONE
                    ph3аt.visibility= View.GONE
                    ph3а.visibility= View.GONE
                    ph4t.visibility= View.GONE
                    ph4.visibility=View.GONE
                    ph4аt.visibility= View.GONE
                    ph4а.visibility= View.GONE
                    ph5t.visibility= View.GONE
                    ph5.visibility=View.GONE
                    ph5аt.visibility= View.GONE
                    ph5а.visibility= View.GONE
                }
                5->{
                    ph3t.visibility= View.VISIBLE
                    ph3.visibility=View.VISIBLE
                    ph3аt.visibility= View.VISIBLE
                    ph3а.visibility= View.VISIBLE
                    ph4t.visibility= View.VISIBLE
                    ph4.visibility=View.VISIBLE
                    ph4аt.visibility= View.VISIBLE
                    ph4а.visibility= View.VISIBLE
                    ph5t.visibility= View.VISIBLE
                    ph5.visibility=View.VISIBLE
                    ph5аt.visibility= View.VISIBLE
                    ph5а.visibility= View.VISIBLE
                }



            }
        }

        poln_ver.setOnClickListener{
            number3answer.text=polnver(count).toString() //вызов функции рассчета полной вероятности
            number3formula.visibility=View.VISIBLE
            number3formula.setBackgroundResource(R.drawable.polnver)
        }

        bay.setOnClickListener{
            if(polnver(count)!=0.0) {
                var PHi = convertPHi(count)
                var PHiA = convertPHiA(count)
                var AnswerList: ArrayList<Double> = arrayListOf(0.0)
                for (i in 0 until count) {
                    AnswerList.add(PHi.get(i) * PHiA.get(i) / polnver(count))
                }
                var Answer = ""
                for (i in 1 until AnswerList.size) {
                    Answer += "PA(H$i)= ${AnswerList.get(i)} \n"
                    number3answer.text = Answer
                }
                number3formula.visibility = View.VISIBLE
                number3formula.setBackgroundResource(R.drawable.bay)
            }
        }

    }

    fun convertPHi (count : Int):ArrayList<Double> {
        var PI: ArrayList<Double> = arrayListOf()

        try {
            PI.add(ph1.text.toString().toDouble())
        } catch (e: NumberFormatException) {
            PI.add(0.0)
        }

        try {
            PI.add(ph2.text.toString().toDouble())
        } catch (e: NumberFormatException) {
            PI.add(0.0)
        }

        if(count>2){
            try {
                PI.add(ph3.text.toString().toDouble())
            } catch (e: NumberFormatException) {
                PI.add(0.0)
            }
        }

        if(count>3){
            try {
                PI.add(ph4.text.toString().toDouble())
            } catch (e: NumberFormatException) {
                PI.add(0.0)
            }
        }

        if(count>4){
            try {
                PI.add(ph5.text.toString().toDouble())
            } catch (e: NumberFormatException) {
                PI.add(0.0)
            }
        }

            return PI
        }

    fun convertPHiA (count : Int):ArrayList<Double> {
        var PI: ArrayList<Double> = arrayListOf()
        try {
            PI.add( if(ph1а.text.toString().toDouble()<1.0) ph1а.text.toString().toDouble() else 1.0)
        } catch (e: NumberFormatException) {
            PI.add(0.0)
        }

        try {
            PI.add( if(ph2а.text.toString().toDouble()<1.0) ph2а.text.toString().toDouble() else 1.0)
        } catch (e: NumberFormatException) {
            PI.add(0.0)
        }

        if(count>2){
            try {
                PI.add( if(ph3а.text.toString().toDouble()<1.0) ph3а.text.toString().toDouble() else 1.0)
            } catch (e: NumberFormatException) {
                PI.add(0.0)
            }
        }

        if(count>3){
            try {
                PI.add( if(ph4а.text.toString().toDouble()<1.0) ph4а.text.toString().toDouble() else 1.0)
            } catch (e: NumberFormatException) {
                PI.add(0.0)
            }
        }

        if(count>4){
            try {
                PI.add( if(ph5а.text.toString().toDouble()<1.0) ph5а.text.toString().toDouble() else 1.0)
            } catch (e: NumberFormatException) {
                PI.add(0.0)
            }
        }
        return PI
    }

    fun polnver (count: Int): Double{
        var PHi = convertPHi(count)
        var PHiA = convertPHiA(count)
        var answer = 0.0
        var sum  = BigDecimal ("0")
        /*for(i in 0 until PHi.size){
            sum =sum.add(BigDecimal(PHi.get(i).toString()))
        }*/
        PHi.forEach{
            sum =sum.add(BigDecimal(it.toString()))
        } //Специально для Никиты

        if(sum.setScale(3,RoundingMode.CEILING).equals(BigDecimal("1.000"))){
            for(i in 0 until count){
                answer+=PHi.get(i)*PHiA.get(i)
            }
        }
        return answer
    }

}