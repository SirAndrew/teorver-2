package com.example.teorver2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class Number2 extends AppCompatActivity implements View.OnClickListener {
    public static int type = 0;
    Button b1;
    Button b2;
    Button b3;
    Button b4;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_number2_choose);
        b1 = (Button)findViewById(R.id.num21);
        b2= (Button)findViewById(R.id.num22);
        b3 = (Button)findViewById(R.id.num23);
        b4= (Button)findViewById(R.id.num24);
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
        b4.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.num21:
                Intent n1 = new Intent(Number2.this, Number2decision.class);
                startActivity(n1);
                type=1;
                break;
            case R.id.num22:
                Intent n2 = new Intent(Number2.this, Number2decision.class);
                startActivity(n2);
                type=2;
                break;
            case R.id.num23:
                Intent n3 = new Intent(Number2.this, Number2decision.class);
                startActivity(n3);
                type=3;
                break;
            case R.id.num24:
                Intent n4 = new Intent(Number2.this, Number2decision.class);
                startActivity(n4);
                type=4;
                break;
        }
    }
}
