package com.example.teorver2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

public class Number2decision extends AppCompatActivity implements View.OnClickListener {

    ImageView formula;
    EditText m1text;
    EditText m2text;
    EditText ntext;
    Button result;
    TextView answer;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_number2decision);
        formula = (ImageView) findViewById(R.id.formula);
        result =(Button) findViewById(R.id.num2result);
        m1text = (EditText) findViewById(R.id.m1);
        m2text = (EditText) findViewById(R.id.m2);
        ntext = (EditText) findViewById(R.id.n);
        answer = (TextView) findViewById(R.id.number2answer);
        result.setOnClickListener(this);
        switch (Number2.type){
            case 1:
                formula.setImageResource(R.drawable.formula1);
                break;
            case 2:
                formula.setImageResource(R.drawable.formula2);
                break;
            case 3:
                formula.setImageResource(R.drawable.formula3);
                break;
            case 4:
                formula.setImageResource(R.drawable.formula4);
                break;
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.num2result:
                int n=Integer.parseInt(ntext.getText().toString());
                int m1=Integer.parseInt(m1text.getText().toString());
                int m2=Integer.parseInt(m2text.getText().toString());

                if(m1> n || m2 > n){
                    Toast toast = Toast.makeText(getApplicationContext(),"Общее число билетов не может быть меньше числа билетов, которые знает студент", Toast.LENGTH_SHORT);
                    toast.show();
                    return;
                }

                BigDecimal asw = new BigDecimal("0");
                BigDecimal PA;
                BigDecimal PB;

                if(m1==0 || m1==1){
                    PA=BigDecimal.valueOf(0);
                }
                else if(m1==2){
                    PA = C(2, m1);
                    PA = PA.multiply(C(1, n - m1));
                    PA = PA.divide(C(3, n), 5, RoundingMode.CEILING);
                }
                else if (m1==n){
                    PA=BigDecimal.valueOf(1);
                }
                else {
                    PA = C(2, m1);
                    PA = PA.multiply(C(1, n - m1));
                    PA = PA.add(C(3, m1));
                    PA = PA.divide(C(3, n), 5, RoundingMode.CEILING);
                }

                if(m2==0 || m2==1){
                    PB=BigDecimal.valueOf(0);
                }
                else if(m2==2){
                    PB =C(2,m2);
                    PB=PB.multiply(C(1,n-m2));
                    PB=PB.divide(C(3,n),5,RoundingMode.CEILING);
                }
                else if (m2==n){
                    PB=BigDecimal.valueOf(1);
                }
                else {
                    PB =C(2,m2);
                    PB=PB.multiply(C(1,n-m2));
                    PB=PB.add(C(3,m2));
                    PB=PB.divide(C(3,n),5,RoundingMode.CEILING);
                }


                switch (Number2.type){
                    case 1:
                        asw=PA.multiply(PB);
                        break;
                    case 2:
                        BigDecimal a = new BigDecimal("1");
                        PB=a.subtract(PB);
                        asw=PA.multiply(PB);
                        break;
                    case 3:
                        BigDecimal b = new BigDecimal("1");
                        b=b.subtract(PB); //!PB
                        BigDecimal c = new BigDecimal("1");
                        c=c.subtract(PA); //!PA
                        BigDecimal d = PA;
                        d=d.multiply(b); //PA*!PB
                        asw=c.multiply(PB);
                        asw=asw.add(d);
                        break;
                    case 4:
                        asw=PA.add(PB); //PA+PB-PA*PB
                        BigDecimal e = PA;
                        e=e.multiply(PB);
                        asw=asw.subtract(e);
                        break;

                }
                answer.setText(asw.toPlainString());
                break;
        }
    }


    BigDecimal fact(int n) {
        BigDecimal a = new BigDecimal("1");
        if (n < 0) {
            a = BigDecimal.valueOf(0);
            return a;
        }
        if (n == 0) {
            a = BigDecimal.valueOf(1);
            return a;
        } else {
            for (long i=2;i<=n;i++){
                a=a.multiply(BigDecimal.valueOf(i));
            }
        }
        return a;
    }

    BigDecimal C(int m,int n){
        BigDecimal a = fact(n);
        a=a.divide(fact(m));
        a=a.divide(fact(n-m));
        return a;
    }
}
