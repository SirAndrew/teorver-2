package com.example.teorver2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Number1 extends AppCompatActivity implements View.OnClickListener {
    EditText Q1;
    EditText Q2;
    EditText Q3;
    EditText Q4;
    EditText Q5;
    Double q1;
    Double q2;
    Double q3;
    Double q4;
    Double q5;

    TextView result;
    Button answer;
    String resultString;
    double resultDouble;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_number1);
        Q1= (EditText) findViewById(R.id.q1);
        Q2= (EditText) findViewById(R.id.q2);
        Q3= (EditText) findViewById(R.id.q3);
        Q4= (EditText) findViewById(R.id.q4);
        Q5= (EditText) findViewById(R.id.q5);
        answer = (Button) findViewById(R.id.answer);
        result = (TextView) findViewById(R.id.result);
        answer.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.answer:
                try {
                    if(Q1.length()>0)
                        q1=Double.valueOf(Q1.getText().toString());
                    else {
                        Toast toast = Toast.makeText(getApplicationContext(),"Введите q1", Toast.LENGTH_SHORT);
                        toast.show();
                        return;
                    }
                    if(q1>1){
                        Toast toast = Toast.makeText(getApplicationContext(),"Вероятность обрыва не может быть больше 1", Toast.LENGTH_SHORT);
                        toast.show();
                        return;
                    }
                }
                catch (NumberFormatException e){
                    Toast toast = Toast.makeText(getApplicationContext(),"Введите корректное число", Toast.LENGTH_SHORT);
                    toast.show();
                    return;
                }

                try {
                    if(Q2.length()>0)
                        q2=Double.valueOf(Q2.getText().toString());
                    else {
                        Toast toast = Toast.makeText(getApplicationContext(),"Введите q2", Toast.LENGTH_SHORT);
                        toast.show();
                        return;
                    }
                    if(q2>1){
                        Toast toast = Toast.makeText(getApplicationContext(),"Вероятность обрыва не может быть больше 1", Toast.LENGTH_SHORT);
                        toast.show();
                        return;
                    }
                }
                catch (NumberFormatException e){
                    Toast toast = Toast.makeText(getApplicationContext(),"Введите корректное число", Toast.LENGTH_SHORT);
                    toast.show();
                    return;
                }

                try{
                    if(Q3.length()>0)
                        q3=Double.valueOf(Q3.getText().toString());
                    else {
                        Toast toast = Toast.makeText(getApplicationContext(),"Введите q3", Toast.LENGTH_SHORT);
                        toast.show();
                        return;
                    }
                    if(q3>1){
                        Toast toast = Toast.makeText(getApplicationContext(),"Вероятность обрыва не может быть больше 1", Toast.LENGTH_SHORT);
                        toast.show();
                        return;
                    }
                }
                catch (NumberFormatException e){
                    Toast toast = Toast.makeText(getApplicationContext(),"Введите корректное число", Toast.LENGTH_SHORT);
                    toast.show();
                    return;
                }

                try {
                    if(Q4.length()>0)
                        q4=Double.valueOf(Q4.getText().toString());
                    else {
                        Toast toast = Toast.makeText(getApplicationContext(),"Введите q4", Toast.LENGTH_SHORT);
                        toast.show();
                        return;
                    }
                    if(q4>1){
                        Toast toast = Toast.makeText(getApplicationContext(),"Вероятность обрыва не может быть больше 1", Toast.LENGTH_SHORT);
                        toast.show();
                        return;
                    }
                }
                catch (NumberFormatException e){
                    Toast toast = Toast.makeText(getApplicationContext(),"Введите корректное число", Toast.LENGTH_SHORT);
                    toast.show();
                    return;
                }
                try {
                    if(Q5.length()>0)
                        q5=Double.valueOf(Q5.getText().toString());
                    else {
                        Toast toast = Toast.makeText(getApplicationContext(),"Введите q5", Toast.LENGTH_SHORT);
                        toast.show();
                        return;
                    }
                    if(q5>1){
                        Toast toast = Toast.makeText(getApplicationContext(),"Вероятность обрыва не может быть больше 1", Toast.LENGTH_SHORT);
                        toast.show();
                        return;
                    }
                }
                catch (NumberFormatException e){
                    Toast toast = Toast.makeText(getApplicationContext(),"Введите корректное число", Toast.LENGTH_SHORT);
                    toast.show();
                    return;
                }

                resultDouble = (q1+q4-q1*q4)*q3*(q2+q5-q2*q5);
                resultString= Double.toString(resultDouble);
                result.setText("Вероятность обрыва цепи: " + resultString);
        }
    }
}
