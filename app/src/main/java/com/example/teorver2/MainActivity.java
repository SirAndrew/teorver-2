package com.example.teorver2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button b1;
    Button b2;
    Button b3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        b1 = (Button)findViewById(R.id.number1);
        b2= (Button)findViewById(R.id.number2);
        b3= (Button) findViewById(R.id.number3);
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.number1:
                Intent n1 = new Intent(MainActivity.this, Number1.class);
                startActivity(n1);
                break;
            case R.id.number2:
                Intent n2 = new Intent(MainActivity.this, Number2.class);
                startActivity(n2);
                break;
            case R.id.number3:
                Intent n3 = new Intent(MainActivity.this, Number3kt.class);
                startActivity(n3);
                break;
        }
    }
}
